#!/bin/bash

dd if=/dev/zero of=~/disk0.img count=10 bs=1M
dd if=/dev/zero of=~/disk1.img count=10 bs=1M
dd if=/dev/zero of=~/disk2.img count=10 bs=1M
dd if=/dev/zero of=~/disk3.img count=10 bs=1M
dd if=/dev/zero of=~/disk4.img count=10 bs=1M


##################################

losetup -d /dev/loop0
losetup -d /dev/loop1
losetup -d /dev/loop2
losetup -d /dev/loop3
losetup -d /dev/loop4

losetup /dev/loop0 ~/disk0.img --sizelimit 10M 
losetup /dev/loop1 ~/disk1.img --sizelimit 10M 
losetup /dev/loop2 ~/disk2.img --sizelimit 10M 
losetup /dev/loop3 ~/disk3.img --sizelimit 10M 
losetup /dev/loop4 ~/disk4.img --sizelimit 10M 

##################################

 
