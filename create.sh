##!/bin/bash

### create ram disks ###
#dd if=/dev/zero of=~/disk0.img count=100 bs=1M
dd if=/dev/zero of=~/disk1.img count=10 bs=1M
dd if=/dev/zero of=~/disk2.img count=20 bs=1M
dd if=/dev/zero of=~/disk3.img count=40 bs=1M
dd if=/dev/zero of=~/disk4.img count=40 bs=1M

#dd if=/dev/zero of=~/disk5.img count=8 bs=1G

##################################

#losetup -d /dev/loop0
losetup -d /dev/loop1
losetup -d /dev/loop2
losetup -d /dev/loop3
losetup -d /dev/loop4
#losetup -d /dev/loop5

sleep 1

#losetup /dev/loop0 ~/disk0.img --sizelimit 100M 
losetup /dev/loop1 ~/disk1.img --sizelimit 10M
losetup /dev/loop2 ~/disk2.img --sizelimit 20M
losetup /dev/loop3 ~/disk3.img --sizelimit 40M
losetup /dev/loop4 ~/disk4.img --sizelimit 40M
#losetup /dev/loop5 ~/disk512M.img --sizelimit 512M
 

################################################
# 1. get max sector per disk
##############################################
#max_sector0=$(blockdev --getsize /dev/loop0)
#max_sector1=$(blockdev --getsize /dev/loop1)
#max_sector2=$(blockdev --getsize /dev/loop2)
#max_sector3=$(blockdev --getsize /dev/loop3)
#max_sector4=$(blockdev --getsize /dev/loop4)



##################################
### 2. create logical disk mapping to physical ###
##################################
#echo "0 $max_sector0 dbg /dev/loop0 0" | dmsetup create vloop0
#echo "0 $max_sector1 dbg /dev/loop1 0" | dmsetup create vloop1
#echo "0 $max_sector2 dbg /dev/loop2 0" | dmsetup create vloop2
#echo "0 $max_sector3 dbg /dev/loop3 0" | dmsetup create vloop3
#echo "0 $max_sector4 dbg /dev/loop4 0" | dmsetup create vloop4



############################################3
### 3. rm existed /dev/md0 and create new one
############################################
rm -rf /dev/md0
#mdadm --create --auto=yes /dev/md0 --level=5 --raid-devices=4 --spare-devices=1 /dev/mapper/vloop{1,2,3,4,0}
#mdadm --create --auto=yes /dev/md0 --level=5 --raid-devices=4 /dev/loop{1,2,3,4}
mdadm --create --auto=yes /dev/md0 --level=5 --raid-devices=4 /dev/loop{4,3,2,1}




 
